# Sources

## Python API Packages

Main Package - https://github.com/swar/nba_api

Endpoint Docs - https://github.com/seemethere/nba_py/wiki/stats.nba.com-Endpoint-Documentation

alt package for pbp stats - https://github.com/dblackrun/pbpstats

## NBA Stats API Tutorial

Players at period - https://github.com/rd11490/NBA_Tutorials/tree/master/players_on_court

PBP Analysis - https://github.com/rd11490/NBA_Tutorials/tree/master/analyze_play_by_play

Parsing PBP - https://github.com/rd11490/NBA_Tutorials/tree/master/play_by_play_parser

## APM/RAPM Calculation

OG RAPM Write Up (Rosebaum) - http://www.82games.com/comm30.htm?utm_source=pocket_mylist

RAPM Python - https://github.com/rd11490/NBA_Tutorials/tree/master/rapm

Deep Dive RAPM - https://squared2020.com/2017/09/18/deep-dive-on-regularized-adjusted-plus-minus-ii-basic-application-to-2017-nba-data-with-r/

python rapm 2020 - https://github.com/vraja2/rapm/blob/master/rapm.ipynb

# General notes

"""
EVENTMSGTYPE Types:

1 -> MAKE
2 -> MISS
3 -> FreeThrow
4 -> Rebound
5 -> Turnover
6 -> Foul
7 -> Violation
8 -> Substitution
9 -> Timeout
10 -> JumpBall
11 -> Ejection
12 -> StartOfPeriod
13 -> EndOfPeriod
14 -> Empty
"""

"""
eventActionType Types: FOULS

% = technical FT
* = FT

FOUL TYPES
 1 - Personal
 2 - Shooting *
 3 - Loose Ball
 4 - Offensive
 5 - Inbound foul *(1 FTA)
 6 - Away from play
 8 - Punch foul %(Technical)
 9 - Clear Path *
 10 - Double Foul
 11 - Technical *%
 12 - Non-Unsportsmanlike (Technical)
 13 - Hanging *%(Technical)
 14 - Flagrant 1 *%
 15 - Flagrant 2 *%
 16 - Double Technical
 17 - Defensive 3 seconds *%(Technical)
 18 - Delay of game
 19 - Taunting *%(Technical)
 25 - Excess Timeout *%(Technical)
 26 - Charge
 27 - Personal Block
 28 - Personal Take
 29 - Shooting Block *
 30 - Too many players *%(Technical)

Offensive fouls: Offensive, Charge

"""
*************************************************************************
"""
eventActionType Types: Rebounds

Rebound Types
0 - Player Rebound
1 - Team Rebound*
Not always labeled properly
"""

*************************************************************************

"""
eventActionType Types: Rebounds

Rebound Types
0 - Player Rebound
1 - Team Rebound*
Not always labeled properly
"""

*************************************************************************

"""
eventActionType Types: Free Throws

Free Throw Types

10 - 1 of 1
11 - 1 of 2
12 - 2 of 2
13 - 1 of 3
14 - 2 of 3
15 - 3 of 3
16 - Technical

"""
