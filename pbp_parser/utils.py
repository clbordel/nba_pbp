import json
import pandas as pd
import urllib3
import os
import requests
import math

from nba_api.stats.static import teams
from nba_api.stats.endpoints import leaguegamefinder

header_data = {
    'Host': 'stats.nba.com',
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
    'Referer': 'stats.nba.com',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.9',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
}

# Constants
event_type = 'EVENTMSGTYPE'
event_subtype = 'EVENTMSGACTIONTYPE'
home_description = 'HOMEDESCRIPTION'
neutral_description = 'NEUTRALDESCRIPTION'
away_description = 'VISITORDESCRIPTION'
period_column = 'PERIOD'
game_clock = 'PCTIMESTRING'
time_elapsed = 'TIME_ELAPSED'
time_elapsed_period = 'TIME_ELAPSED_PERIOD'
player1_id = 'PLAYER1_ID'
player1_team_id = 'PLAYER1_TEAM_ID'
player2_id = 'PLAYER2_ID'

def calculate_time_at_period(period):
    if period > 5:
        return (720 * 4 + (period - 5) * (5 * 60)) * 10
    else:
        return (720 * (period - 1)) * 10

# api url for game id pbp data
def play_by_play_url(game_id):
    return "https://stats.nba.com/stats/playbyplayv2/?gameId={0}&startPeriod=0&endPeriod=14".format(game_id)

# api url for boscore data
def advanced_boxscore_url(game_id, start, end):
    return "https://stats.nba.com/stats/boxscoreadvancedv2/?gameId={0}&startPeriod=0&endPeriod=14&startRange={1}&endRange={2}&rangeType=2".format(game_id, start, end)

# Given a period and game, get players who played during period
def get_players_during_period(game_id,period):
    
    low = calculate_time_at_period(period) + 5
    high = calculate_time_at_period(period + 1) - 5
    boxscore_url = advanced_boxscore_url(game_id, low, high)

    #boxscore_players = extract_data(boxscore)[['PLAYER_NAME', 'PLAYER_ID', 'TEAM_ABBREVIATION']]
    boxscore = requests.get(boxscore_url,headers=header_data).json()['resultSets'][0]
    boxscore_players = pd.DataFrame(boxscore['rowSet'],columns=boxscore['headers'])
    return boxscore_players[['PLAYER_NAME', 'PLAYER_ID', 'TEAM_ABBREVIATION','START_POSITION']]

def get_pbp_data(game_id):
    
    pbp_url = play_by_play_url(game_id)

    #boxscore_players = extract_data(boxscore)[['PLAYER_NAME', 'PLAYER_ID', 'TEAM_ABBREVIATION']]
    pbp = requests.get(pbp_url,headers=header_data).json()['resultSets'][0]
    pbp = pd.DataFrame(pbp['rowSet'],columns=pbp['headers'])
    return pbp

# format subs
def split_subs(df, tag):
    subs = df[[tag, 'PERIOD', 'EVENTNUM']]
    subs['SUB'] = tag
    subs.columns = ['PLAYER_ID', 'PERIOD', 'EVENTNUM', 'SUB']
    return subs

# Give pbp data, get only the subs
def get_subs_only(frame):
    substitutionsOnly = frame[frame["EVENTMSGTYPE"] == 8][['PERIOD', 'EVENTNUM', 'PLAYER1_ID', 'PLAYER2_ID']]
    substitutionsOnly.columns = ['PERIOD', 'EVENTNUM', 'OUT', 'IN']
    
    subs_in = split_subs(substitutionsOnly, 'IN')
    subs_out = split_subs(substitutionsOnly, 'OUT')
    
    full_subs = pd.concat([subs_out, subs_in], axis=0).reset_index()[['PLAYER_ID', 'PERIOD', 'EVENTNUM', 'SUB']]
    
    return full_subs

# Get players who's first event was a sub in
def get_first_sub_players(subs):
    first_event_of_period = subs.loc[subs.groupby(by=['PERIOD', 'PLAYER_ID'])['EVENTNUM'].idxmin()]
    players_subbed_in_at_each_period = first_event_of_period[first_event_of_period['SUB'] == 'IN'][['PLAYER_ID', 'PERIOD', 'SUB']]
    return players_subbed_in_at_each_period

# Get a team id from abbreviation (i.e. HOU)
def get_team_id(team_abbv):
    nba_teams = teams.get_teams()
    nba_teams = pd.DataFrame(nba_teams)
    team_lookup = nba_teams[['abbreviation','id']].set_index("abbreviation").to_dict()['id']
    
    return team_lookup[team_abbv]

def get_subs_only(frame):
    substitutionsOnly = frame[frame["EVENTMSGTYPE"] == 8][['PERIOD', 'EVENTNUM', 'PLAYER1_ID', 'PLAYER2_ID']]
    substitutionsOnly.columns = ['PERIOD', 'EVENTNUM', 'OUT', 'IN']

    return substitutionsOnly

def get_start_of_period_players(game_id,period,pbp):
    players_during_period = get_players_during_period(game_id,period)
    all_players = set(players_during_period.PLAYER_ID.unique())
    
    #pbp = get_pbp_data(game_id)
    subs = get_subs_only(pbp)
    subs = subs[subs['PERIOD'] == period]
    
    idxs = subs.index.values
    
    previously_in = []
    for idx in idxs:
        out_player = subs['OUT'][idx]
        previously_in.append(out_player)
        in_player = subs['IN'][idx]
        if in_player not in previously_in:
            if in_player in all_players:
                all_players.remove(in_player)
            
    players_during_period = players_during_period[players_during_period['PLAYER_ID'].isin(all_players)]
    
    team_ids = players_during_period.TEAM_ABBREVIATION.unique()
    team_id_1 = team_ids[0]
    team_id_2 = team_ids[1]
    
    team_1_players = players_during_period[players_during_period['TEAM_ABBREVIATION'] == team_id_1].PLAYER_ID.unique()
    team_2_players = players_during_period[players_during_period['TEAM_ABBREVIATION'] == team_id_2].PLAYER_ID.unique()
    
    data = {'TEAM_ID_1':get_team_id(team_id_1),'TEAM_1_PLAYERS':team_1_players,'TEAM_ID_2':get_team_id(team_id_2),'TEAM_2_PLAYERS':team_2_players,'PERIOD':period}
    
    return data

def get_game_start_master(num_periods,game_id,pbp):
    game_start_master = pd.DataFrame(columns=['TEAM_ID_1','TEAM_1_PLAYERS','TEAM_ID_2','TEAM_2_PLAYERS','PERIOD'])
    for i in range(int(num_periods)):
        period = i+1
        row = get_start_of_period_players(game_id,period,pbp)
        game_start_master = game_start_master.append(row,ignore_index=True)
    
    return game_start_master

# returns a list of nba teams
def get_nba_team_ids():
    nba_teams = teams.get_teams()
    nba_teams = pd.DataFrame(nba_teams)
    team_ids = set(nba_teams.id.values)
    return team_ids


def get_game_ids(season):
    gamefinder = leaguegamefinder.LeagueGameFinder(season_nullable=season)
    games = gamefinder.get_data_frames()[0]

    # get a list of team ids of nba teams and drop if not in it
    nba_team_ids = get_nba_team_ids()
    games = games.groupby('GAME_ID').filter(lambda g: (g.TEAM_ID.isin(nba_team_ids)).all())

    return set(games['GAME_ID'].unique())

def is_made_shot(row):
    return row[event_type] == 1


def is_missed_shot(row):
    return row[event_type] == 2


def is_free_throw(row):
    return row[event_type] == 3


def is_rebound(row):
    return row[event_type] == 4


def is_turnover(row):
    return row[event_type] == 5


def is_foul(row):
    return row[event_type] == 6


def is_violation(row):
    return row[event_type] == 7


def is_substitution(row):
    return row[event_type] == 8


def is_timeout(row):
    return row[event_type] == 9


def is_jump_ball(row):
    return row[event_type] == 10


def is_ejection(row):
    return row[event_type] == 11


def is_start_of_period(row):
    return row[event_type] == 12


def is_end_of_period(row):
    return row[event_type] == 13

def is_miss(row):
    miss = False
    if row[home_description]:
        miss = miss or 'miss' in row[home_description].lower()
    if row[away_description]:
        miss = miss or 'miss' in row[away_description].lower()
    return miss

def is_shooting_foul(row):
    return is_foul(row) and row[event_subtype] == 2


def is_away_from_play_foul(row):
    return is_foul(row) and row[event_subtype] == 6


def is_inbound_foul(row):
    return is_foul(row) and row[event_subtype] == 5


def is_loose_ball_foul(row):
    return is_foul(row) and row[event_subtype] == 3

def is_team_rebound(row):
    return is_rebound(row) and (row[event_subtype] == 1 or math.isnan(row['PLAYER1_TEAM_ID']))


def is_defensive_rebound(ind, row, rows):
    if not is_rebound(row):
        return False
    shot = extract_missed_shot_for_rebound(ind, rows)
    if is_team_rebound(row):
        return shot['PLAYER1_TEAM_ID'] != row['PLAYER1_ID']
    else:
        return shot['PLAYER1_TEAM_ID'] != row['PLAYER1_TEAM_ID']

def extract_missed_shot_for_rebound(ind, rows):
    subset_of_rows = rows[max(0, ind - 10): ind]
    subset_of_rows.reverse()
    for r in subset_of_rows:
        if is_miss(r[1]) or is_missed_free_throw(r[1]):
            return r[1]
    return subset_of_rows[-1][1]

def is_missed_free_throw(row):
    return is_free_throw(row) and is_miss(row)


def is_1_of_1(row):
    return is_free_throw(row) and row[event_subtype] == 10


def is_2_of_2(row):
    return is_free_throw(row) and row[event_subtype] == 12


def is_3_of_3(row):
    return is_free_throw(row) and row[event_subtype] == 15


def is_technical(row):
    return is_free_throw(row) and row[event_subtype] == 13


def is_last_free_throw(row):
    return is_1_of_1(row) or is_last_multi_free_throw(row)


def is_last_multi_free_throw(row):
    return is_2_of_2(row) or is_3_of_3(row)

def is_last_free_throw_made(ind, row, rows):
    if not is_free_throw(row):
        return False
    foul = extract_foul_for_last_freethrow(ind, row, rows)
    return (is_last_multi_free_throw(row) or (
        is_1_of_1(row) and not is_away_from_play_foul(foul) and not is_loose_ball_foul(foul) and not is_inbound_foul(
            foul))) and not is_miss(row)


def extract_foul_for_last_freethrow(ind, row, rows):
    subset_of_rows = rows[max(0, ind - 10): ind]
    subset_of_rows.reverse()
    for r in subset_of_rows:
        if is_foul(r[1]):
            return r[1]
    print(ind)
    print(row)
    return subset_of_rows[0][1]

def is_and_1(ind, row, rows):
    if not is_made_shot(row):
        return False
    # check next 20 events after the make
    subset_of_rows = rows[ind + 1: min(ind + 20, len(rows))]
    cnt = 0
    for sub_ind, r in subset_of_rows:
        # We are looking for fouls or 1 of 1 free throws that happen within 10 seconds of the made shot.
        # We also need to make sure those 1 of 1s are the result of a different type of foul that results in 1 FT.
        # If we have both a foul and a 1 of 1 ft that meet these conditions we can safely assume this shot resulted in
        # an And-1
        if (is_foul(r) or is_1_of_1(r)) and row[time_elapsed] <= r[time_elapsed] <= row[time_elapsed] + 10:
            if is_foul(r) and not is_technical(r) and not is_loose_ball_foul(r) and not is_inbound_foul(r) and r[
                player2_id] == row[player1_id]:
                cnt += 1
            elif is_1_of_1(r) and r[player1_id] == row[player1_id]:
                cnt += 1
    return cnt == 2

def is_make_and_not_and_1(ind, row, rows):
    return is_made_shot(row) and not is_and_1(ind, row, rows)

def is_three(row):
    three = False
    if row[home_description]:
        three = three or '3PT' in row[home_description]
    if row[away_description]:
        three = three or '3PT' in row[away_description]
    return three

def is_team_turnover(row):
    return is_turnover(row) and (is_5_second_violation(row) or is_8_second_violation(row) or is_shot_clock_violation(row) or is_too_many_players_violation(row))

def is_5_second_violation(row):
    return is_turnover(row) and row[event_subtype] == 9

def is_8_second_violation(row):
    return is_turnover(row) and row[event_subtype] == 10

def is_shot_clock_violation(row):
    return is_turnover(row) and row[event_subtype] == 11

def is_too_many_players_violation(row):
    return is_turnover(row) and row[event_subtype] == 44

# We will need to know the game clock at each event later on. Let's take the game clock string (7:34) and convert it into seconds elapsed
def parse_time_elapsed(time_str, period):
    # Maximum minutes in a period is 12 unless overtime
    max_minutes = 12 if period < 5 else 5
    # Split string on :
    [minutes, sec] = time_str.split(':')
    # extract minutes and seconds
    minutes = int(minutes)
    sec = int(sec)

    # 7:34 (4 minutes 26 seconds have passed) -> 12 - 7 -> 5, need to subtract an extra minute.
    min_elapsed = max_minutes - minutes - 1
    sec_elapsed = 60 - sec

    return (min_elapsed * 60) + sec_elapsed

# We will also need to calculate the total time elapsed, not just the time elapsed in the period
def calculate_time_elapsed(row):
    # Caclulate time elapsed in the period
    time_in_period = calculate_time_elapsed_period(row)
    period = row[period_column]
    # Calculate total time elapsed up to the start of the current period
    if period > 4:
        return (12 * 60 * 4) + ((period - 5) * 5 * 60) + time_in_period
    else:
        return ((period - 1) * 12 * 60) + time_in_period

# method for calculating time elapsed in a period from a play by play event row
def calculate_time_elapsed_period(row):
    return parse_time_elapsed(row[game_clock], row[period_column])
        
def is_end_of_possession(ind, row, rows):
    return is_turnover(row) or (is_last_free_throw_made(ind, row, rows)) or is_defensive_rebound(ind, row, rows) or \
           is_make_and_not_and_1(ind, row, rows) or is_end_of_period(row)

# We need to count up each teams points from a possession
def count_points(possession):
    # points will be a map where the key is the team id and the value is the number of points scored in that possesion
    points = {}
    for p in possession:
        if is_made_shot(p) or (not is_miss(p) and is_free_throw(p)):
            if p['PLAYER1_TEAM_ID'] in points:
                points[p['PLAYER1_TEAM_ID']] += extract_points(p)
            else:
                points[p['PLAYER1_TEAM_ID']] = extract_points(p)
    return points

# We need to know how many points each shot is worth:
def extract_points(p):
    if is_free_throw(p) and not is_miss(p):
        return 1
    elif is_made_shot(p) and is_three(p):
        return 3
    elif is_made_shot(p) and not is_three(p):
        return 2
    else:
        return 0
    
def determine_possession_team(p, team1, team2):
    if is_made_shot(p) or is_free_throw(p):
        return str(int(p['PLAYER1_TEAM_ID']))
    elif is_rebound(p):
        if is_team_rebound(p):
            if p['PLAYER1_ID'] == team1:
                return team2
            else:
                return team1
        else:
            if p['PLAYER1_TEAM_ID'] == team1:
                return team2
            else:
                return team1
    elif is_turnover(p):
        if is_team_turnover(p):
            return str(int(p['PLAYER1_ID']))
        else:
            return str(int(p['PLAYER1_TEAM_ID']))
    else:
        if math.isnan(p['PLAYER1_TEAM_ID']):
            return str(int(p['PLAYER1_ID']))
        else:
            return str(int(p['PLAYER1_TEAM_ID']))
