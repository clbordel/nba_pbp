import json
import pandas as pd
import urllib3
import os
import requests
import math
import glob
import argparse

# Custom lib
from utils import *

parser = argparse.ArgumentParser(description='NBA PBP Parser')
parser.add_argument('-s','--season', help='season nullable arg i.e. "2020-21"', required=True)
clargs = parser.parse_args()

def update_subs(row):
    period = row['PERIOD']
    # If the event is a substitution we need to sub out the players on the court
    if is_substitution(row):
        team_id = row['PLAYER1_TEAM_ID']
        player_in = str(row['PLAYER2_ID'])
        player_out = str(row['PLAYER1_ID'])
        players = sub_map[period][team_id]
        players = list(players)
        players = [str(x) for x in players]
        players_index = players.index(player_out)
        players[players_index] = player_in
        players.sort()
        sub_map[period][team_id] = players

    for i, k in enumerate(sub_map[period].keys()):
        row['TEAM{}_ID'.format(i + 1)] = k
        row['TEAM{}_PLAYER1'.format(i + 1)] = sub_map[period][k][0]
        row['TEAM{}_PLAYER2'.format(i + 1)] = sub_map[period][k][1]
        row['TEAM{}_PLAYER3'.format(i + 1)] = sub_map[period][k][2]
        row['TEAM{}_PLAYER4'.format(i + 1)] = sub_map[period][k][3]
        row['TEAM{}_PLAYER5'.format(i + 1)] = sub_map[period][k][4]

    return row

def parse_possession(rows):
    possessions = []
    current_posession = []
    for ind, row in rows:
        # update our subs
        row = update_subs(row)
        # No need to include subs or end of period events in our possession list
        if not is_substitution(row) and not is_end_of_period(row):
            current_posession.append(row)
        # if the current event is the last event of a possession, add the current possession to our list of possessions
        # and start a new possession
        if is_end_of_possession(ind, row, rows):
            # No need to add empty end of period possessions
            if len(current_posession) > 0:
                possessions.append(current_posession)
            current_posession = []
    return possessions

def build_possession(possession):
    
    #times_of_events = [p[time_elapsed] for _,p in possession]
    times_of_events = [p[time_elapsed] for p in possession]
    possession_start = min(times_of_events)
    possession_end = max(times_of_events)
    points = count_points(possession)
    game_id = possession[0]['GAME_ID']
    period = possession[0][period_column]

    team1_id = possession[0]['TEAM1_ID']
    team1_player1 = possession[0]['TEAM1_PLAYER1']
    team1_player2 = possession[0]['TEAM1_PLAYER2']
    team1_player3 = possession[0]['TEAM1_PLAYER3']
    team1_player4 = possession[0]['TEAM1_PLAYER4']
    team1_player5 = possession[0]['TEAM1_PLAYER5']
    team1_points = points[team1_id] if team1_id in points else 0

    team2_id = possession[0]['TEAM2_ID']
    team2_player1 = possession[0]['TEAM2_PLAYER1']
    team2_player2 = possession[0]['TEAM2_PLAYER2']
    team2_player3 = possession[0]['TEAM2_PLAYER3']
    team2_player4 = possession[0]['TEAM2_PLAYER4']
    team2_player5 = possession[0]['TEAM2_PLAYER5']
    team2_points = points[team2_id] if team2_id in points else 0

    possession_team = determine_possession_team(possession[-1], team1_id, team2_id)

    return {
        'team1_id': str(team1_id),
        'team1_player1': str(team1_player1),
        'team1_player2': str(team1_player2),
        'team1_player3': str(team1_player3),
        'team1_player4': str(team1_player4),
        'team1_player5': str(team1_player5),
        'team2_id': str(team2_id),
        'team2_player1': str(team2_player1),
        'team2_player2': str(team2_player2),
        'team2_player3': str(team2_player3),
        'team2_player4': str(team2_player4),
        'team2_player5': str(team2_player5),
        'game_id': str(game_id),
        'period': period,
        'possession_start': possession_start,
        'possession_end': possession_end,
        'team1_points': team1_points,
        'team2_points': team2_points,
        'possession_team': str(possession_team)
    }

def get_games_done(season):
    gd = glob.glob(f"/home/cody/workspace/nba_pbp/data/{season}/*/*_parsed_pbp.csv")  
    games_done = []
    for g in gd:
        g = g.split("/")[8]
        g = g.split("_")[0]
        games_done.append(g)
    return set(games_done)

if __name__ == "__main__":

    print("*****************************************")
    games_done = get_games_done(clargs.season)
    game_ids = get_game_ids(clargs.season)
    print(f"{len(game_ids)} total games - {len(games_done)} games done")
    game_ids = game_ids - games_done
    print(f"Finishing {len(game_ids)} games")
    print("*****************************************")

    # games_to_ignore = set(["0022100975","0022100979","0011900033","0011900067","0011900011","0011900025","0011900052",\
    # "0011900008","0011900009","0011900020","0011900001","0011900012","0011900018","0011900004","0011900032","0011900047",\
    # "0022000853","0022100943","0022100945","0012100014","0012100014","0022100665"])
    games_to_ignore = set(["0022000285","0022000853","0022000215"])
    
    game_ids = game_ids - games_to_ignore

    for game_id in game_ids:

        print(game_id)

        # Create output dir if not exists
        outdir = f'/home/cody/workspace/nba_pbp/data/{clargs.season}/{game_id}'
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        
        # Get pbp data for game
        try:
            play_by_play = get_pbp_data(game_id)
        except Exception as e:
            print(e)
            continue

        # Get num periods in game
        periods = play_by_play.PERIOD.max()
        
        # get players at start of period - HALF WAY POINT
        try:
            players_at_start_of_period = get_game_start_master(periods,game_id,play_by_play)
        except Exception as e:
            print(e)
            continue

        # backup the start master to make sure it looks correct
        players_at_start_of_period.to_csv(os.path.join(outdir,f"{game_id}_game_start_master.csv"))

        play_by_play[home_description] = play_by_play[home_description].fillna("")
        play_by_play[neutral_description] = play_by_play[home_description].fillna("")
        play_by_play[away_description] = play_by_play[away_description].fillna("")

        play_by_play[time_elapsed] = play_by_play.apply(calculate_time_elapsed, axis=1)
        play_by_play[time_elapsed_period] = play_by_play.apply(calculate_time_elapsed_period, axis=1)

        sub_map = {}
        # Pre-populate the map with the players at the start of each period
        for row in players_at_start_of_period.iterrows():
            sub_map[row[1]['PERIOD']] = {row[1]['TEAM_ID_1']: row[1]['TEAM_1_PLAYERS'],
                                        row[1]['TEAM_ID_2']: row[1]['TEAM_2_PLAYERS']}

        pbp_rows = list(play_by_play.iterrows())
        possessions = parse_possession(pbp_rows)

        # Build a list of parsed possession objects
        parsed_possessions = []
        for possession in possessions:
            parsed_possessions.append(build_possession(possession))

        # Build a dataframe from the list of parsed possession
        df = pd.DataFrame(parsed_possessions)

        file_out_path = f"{game_id}_parsed_pbp.csv"
            
        fp_final = os.path.join(outdir,file_out_path)
        print(fp_final)

        df.to_csv(fp_final)
