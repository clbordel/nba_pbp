import json
import pandas as pd
import urllib3
import os
import requests
import math
import glob
import argparse

# Custom lib
from utils import *

parser = argparse.ArgumentParser(description='NBA PBP Parser')
parser.add_argument('-s','--season', help='season nullable arg i.e. "2020-21"', required=True)

def get_games_done(season):
    gd = glob.glob(f"/home/cody/workspace/nba_pbp/data/{season}/*/*_parsed_stint.csv")  
    games_done = []
    for g in gd:
        g = g.split("/")[8]
        g = g.split("_")[0]
        games_done.append(g)
    return set(games_done)

if __name__ == "__main__":

    clargs = parser.parse_args()

    print("*****************************************")
    games_done = get_games_done(clargs.season)
    game_ids = get_game_ids(clargs.season)
    print(f"{len(game_ids)} total games - {len(games_done)} games done")
    game_ids = game_ids - games_done
    print(f"Finishing {len(game_ids)} games")
    print("*****************************************")

    # games_to_ignore = set(["0022100960","0011900033","0011900067","0011900011","0011900025","0011900052","0011900008","0011900009",\
    # "0011900020","0011900001","0011900012","0011900018","0011900004","0011900032","0011900047","0022000853",\
    # "0022100943","0022100945","0012100014","0012100014","0022100665","0022100979","0022100947","0022100947","0022100964"])
    games_to_ignore = set(["0022000285","0022000853","0022000215","0022101021","0022101020","0022101022","0012100014","0022101023","0022101024"])

    game_ids = game_ids - games_to_ignore

    for game_id in game_ids:

        print(game_id)

        df = pd.read_csv(f"/home/cody/workspace/nba_pbp/data/{clargs.season}/{game_id}/{game_id}_parsed_pbp.csv")

        df2 = df.shift(-1).fillna(0).astype(int)

        cols_to_compare = ['team1_player1', 'team1_player2',
       'team1_player3', 'team1_player4', 'team1_player5', 'team2_id',
       'team2_player1', 'team2_player2', 'team2_player3', 'team2_player4',
       'team2_player5','period']

        df['new_stint'] = (df[cols_to_compare] == df2[cols_to_compare]).all(axis='columns')

        # These are our "flags" to mark new stints
        new_stints_flags = df[df['new_stint'] == False]
        new_stints_flags = new_stints_flags.append(df.loc[0],ignore_index=True)
        new_stints_flags = new_stints_flags.sort_values("possession_start")
        new_stints_flags['next_stint_start'] = new_stints_flags['possession_start'].shift(-1).fillna(10000000).astype(int)

        # An empty df to build our stint data
        stint_pbp = pd.DataFrame(columns={'t1_id','t2_id','t1_players','t2_players','stint_start','stint_end','t1_points_scored','t2_points_scored','t1_pm','t2_pm'})

        for flag in new_stints_flags.iterrows():
            
            flag = flag[1]
            #print(flag['team1_id'])
            this_stint_start = flag['possession_start']
            this_stint_end = flag['next_stint_start'] - 1
            
            stint_group = df[df['possession_start'] >= this_stint_start]
            stint_group = df[df['possession_end'] <= this_stint_end]
            
            # Grab basic data from the row
            t1_id = flag['team1_id']
            t2_id = flag['team2_id']
            t1_players = [flag['team1_player1'],flag['team1_player2'],flag['team1_player3'],flag['team1_player4'],flag['team1_player5']]
            t2_players = [flag['team2_player1'],flag['team2_player2'],flag['team2_player3'],flag['team2_player4'],flag['team2_player5']]

            t1_points_chg = flag['team1_points']
            t2_points_chg = flag['team2_points']
            team_in_poss = flag['possession_team']
            
            # Basic p/m calc numbers needed
            t1_points_scored = stint_group['team1_points'].sum()
            t2_points_scored = stint_group['team2_points'].sum()
            
            t1_pm = t1_points_scored - t2_points_scored
            t2_pm = t2_points_scored - t1_points_scored
            
            num_poss = stint_group.shape[0]
            
            margin_per_100 = (t1_pm/num_poss) * 100

            print("***************************")
            print(f"{num_poss} possessions")
            print(f"Team 1 players with {t1_pm} P/M")
            print(t1_players)
            print(f"Team 2 players with {t2_pm} P/M")
            print(t2_players)
            print("***************************")
            
            new_row = {'t1_id':t1_id,'t2_id':t2_id,'t1_players':t1_players,'t2_players':t2_players,'stint_start':this_stint_start,'stint_end':this_stint_end,'t1_points_scored':t1_points_scored,'t2_points_scored':t2_points_scored,'t1_pm':t1_pm,'t2_pm':t2_pm,'num_poss':num_poss,'margin_per_100':margin_per_100}
            stint_pbp = stint_pbp.append(new_row,ignore_index=True)

        path = f"/home/cody/workspace/nba_pbp/data/{clargs.season}/{game_id}/{game_id}_parsed_stint.csv"
        print(path)
        stint_pbp.to_csv(path)

        
