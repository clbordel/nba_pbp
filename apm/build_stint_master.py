import json
import pandas as pd
import urllib3
import os
import requests
import math
import glob
import argparse

parser = argparse.ArgumentParser(description='NBA PBP Parser')
parser.add_argument('-s','--season', help='season nullable arg i.e. "2020-21"', required=True)
parser.add_argument('-g','--game-id', help='If you want to produce a stint master for a specific game')

if __name__ == '__main__':

    clargs = parser.parse_args()  
        
    if clargs.season == None and clargs.game_id == None:
        files = glob.glob(f"/home/cody/workspace/nba_pbp/data/*/*/*_parsed_stint.csv")
    elif clargs.game_id == None:
        files = glob.glob(f"/home/cody/workspace/nba_pbp/data/{clargs.season}/*/*_parsed_stint.csv")
    else:
        files = [f"/home/cody/workspace/nba_pbp/data/{clargs.season}/{clargs.game_id}/{clargs.game_id}_parsed_stint.csv"]

    # For each game
    for f in files:
        
        # Read in and format 
        df = pd.read_csv(f)
        df['t1_players'] = df['t1_players'].str.strip("[").str.strip("]")
        df['t2_players'] = df['t2_players'].str.strip("[").str.strip("]")

        df['t1_players'] = df['t1_players'].str.split(",")
        df['t2_players'] = df['t2_players'].str.split(",")
        
        df['t1_players'] = df['t1_players'].apply(lambda x: [int(y.strip()) for y in x])
        df['t2_players'] = df['t2_players'].apply(lambda x: [int(y.strip()) for y in x])

        # Get the unique players for each team
        unique_t1_players = list(df['t1_players'].explode().unique())
        unique_t2_players = list(df['t2_players'].explode().unique())
        
        # Ensure that all players are in the master df
        for player in unique_t1_players + unique_t2_players:
            if player in master_df.columns:
                continue
            else:
                master_df[player] = 0
                
        # iterate each row and update the master df
        indices = df.index.values
        for idx in indices:
            t1_players = df['t1_players'][idx]
            t2_players = df['t2_players'][idx]
            
            # get point diff
            pt_diff = df['margin_per_100'][idx]
            
            # add row of all zeros + point diff
            new_row = pd.Series(0, index=master_df.columns)
            new_row['diff_per_100'] = pt_diff

            # add in player marks
            for t1_player in t1_players:
                #t1_player = str(t1_player)
                new_row[t1_player] = 1
            for t2_player in t2_players:
                #t2_player = str(t2_player)
                new_row[t2_players] = -1
            
            master_df = master_df.append(new_row, ignore_index=True)
            master_df = master_df.fillna(0)

    if clargs.game_id == None:
        path = f'/home/cody/workspace/nba_pbp/data/{clargs.season}/{clargs.season}-stint_matrix.csv'
        master_df.to_csv(path)